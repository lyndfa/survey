// include gulp
var gulp = require('gulp');

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');

// JS concat, strip debugging and minify
gulp.task('scripts', function() {
    gulp.src([
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/Materialize/dist/js/materialize.min.js'
        ])
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./'));
});

// CSS concat, auto-prefix and minify
gulp.task('styles', function() {
    gulp.src([
            'bower_components/Materialize/dist/css/materialize.min.css'
        ])
        .pipe(concat('styles.css'))
        .pipe(autoprefix('last 2 versions'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./'));
});

// default gulp task
gulp.task('default', ['scripts', 'styles'], function() {
});
