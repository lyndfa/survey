<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lyndfa</title>

    <link rel="stylesheet" href="bower_components/Materialize/dist/css/materialize.min.css">
    <link rel="stylesheet" href="default.css">
    <link rel="stylesheet" href="home.css">
</head>
<body>
    <section id="main_container">
        <div class="logo_holder">
            <img src="main_logo.png" title="لیندا به زبان فارسی" alt="لیندا به زبان فارسی">
            <h1>ویدئوهای لیندا به زبان فارسی</h1>
            <p class="intro_info">به <span>لیندفـا</span> در انتخاب آموزش هایی که نیاز دارید کمک کنید و<br> اولین کسی باشید که از آغاز به کار لیندفا مطلع می شود</p>
            <a class="go_to_survey" href="survey.html">نظرسنجی و پیش ثبت نام</a>
        </div>
        <div class="coming_soon">به زودی...</div>
    </section>

    <!--<a title="Web Analytics" href="http://clicky.com/100926518"><img alt="Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
    <script src="//static.getclicky.com/js" type="text/javascript"></script>
    <script type="text/javascript">try{ clicky.init(100926518); }catch(e){}</script>
    <noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100926518ns.gif" /></p></noscript>-->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-74180124-1', 'auto');
      ga('send', 'pageview');

    </script>
</body>
</html>