(function () {
    'use strict';
    var currentStep = 0;

    $(document).ready(function () {
        currentStep = 0;

        $("select").change(function () {
            $(this).siblings("input").addClass("filled-input")
            if (0 != currentStep) {
                var count = 0;
                $("#step_" + currentStep).find("select").each(function () {
                    if($(this).siblings("input").hasClass("filled-input"))
                        count++;
                });
                if(3 == currentStep && 5 == count || 6 == count) {
                    $('#reward').html(calcuateReward());
                    $("#survey_reward").slideDown();
                }
            }
        }).material_select();

        $("#name").keyup(function () {
            $("#no_name").slideUp();
            $("#name").removeClass("invalid");
        });

        $("#email").keyup(function () {
            $("#no_email, #bad_email").slideUp();
            $("#email").removeClass("invalid");
            var e = new RegExp(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.[A-Z0-9]{2,6}([-.][A-Z0-9]{2,6})*$/i);
            if(e.test($("#email").val()))
                $("#email").addClass("valid");
        });

        $('#start_survey').click(function () {
            $("#register_end").hide();
            currentStep=1;
            $("#step_" + currentStep).show();
            $(".survey_footer").fadeIn();
        });

        $("#next_page").click(function () {
            if("block" == $("#survey_reward").css("display"))
                $("#survey_reward").slideUp();

            switch (currentStep + 1) {
                case 1:
                    if(checkFirstStep())
                        return register(true);
                    else return;
                case 2:
                    $(".prev_page_wrapper").fadeIn();
                    break;
                case 3:
                    $("#next_page").fadeOut();
                    break;
                default:
                    return
            }
            $("#step_" + currentStep).hide();
            currentStep++;
            $("#step_" + currentStep).show();
            $('html, body').scrollTop(0);
        });

        $("#prev_page").click(function () {
            switch (currentStep - 1) {
                case 1:
                    $(".prev_page_wrapper").fadeOut();
                    break;
                case 2:
                    $("#next_page").fadeIn();
                    break;
                default:
                    return
            }
            $("#step_" + currentStep).hide();
            currentStep--;
            $("#step_" + currentStep).show();
        });

        $("#send_survey").click(function () {
            if(0 == currentStep) {
                if(checkFirstStep())
                    register(true);
                else
                    return;
            }
            else {
                $(".survey_overlay").css("display", "flex").hide().fadeIn();
                $.ajax("survey.php", {
                    method: "POST",
                    data: {
                        email: $("#email").val(),
                        q1: $("#q11").val().join(','),
                        q2: $("#q12").val(),
                        q3: $("#q13").val().join(','),
                        q4: $("#q14").val(),
                        q5: $("#q15").val().join(','),
                        q6: $("#q16").val(),
                        q7: $("#q21").val().join(','),
                        q8: $("#q22").val(),
                        q9: $("#q23").val(),
                        q10: $("#q24").val(),
                        q11: $("#q25").val(),
                        q12: $("#q26").val(),
                        q13: $("#q31").val(),
                        q14: $("#q32").val(),
                        q15: $("#q33").val(),
                        q16: $("#q34").val(),
                        q17: $("#q35").val(),
                        q18: $("#q36").val()
                    }
                }).done(function () {
                    $("#step_" + currentStep).hide();
                    $(".prev_page_wrapper").fadeIn();
                    var e = 0;
                    $("#q11, #q12, #q13, #q14, #q15, #q16, #q21, #q22, #q23, #q24, #q25, #q26, #q31, #q32, #q33, #q34, #q35, #q36").each(function () {
                        if($(this).val() && 0 != $(this).val().length )
                            e++;
                    });
                    e = Math.floor(e / 6);
                    e *= 1e3;
                    $("#survey_reward_amount").html(e);
                    $("#survey_end").show();
                    $(".survey_overlay").fadeOut();
                    $(".survey_footer").fadeOut(function () {
                        $('.g-recaptcha-wrapper').hide();
                    });
                });
            }
        });
    });

    // e == true if it's first step
    function register(e) {
        $(".survey_overlay").css("display", "flex").hide().fadeIn();
        $.ajax("register.php", {
            method: "POST",
            dataType: "html",
            async: !0,
            data: {
                captchaResponse: $("#g-recaptcha-response").val(),
                name: $("#name").val(),
                email: $("#email").val(),
                gender: $("#male").prop("checked") ? 1 : $("#female").prop("checked") ? 2 : 0,
                age: $("#age").val()
            }
        }).done(function () {
            $(".survey_overlay").fadeOut();
            if(!e) {
                $(".g-recaptcha-wrapper").slideUp();
                $("#next_page").html('<i class="mdi-content-send right"></i>سوالات بعدی');
                $("#send_survey").html("ثبت و اتمام نظر سنجی");
                $("#step_" + currentStep).hide();
                currentStep++;
                $("#step_" + currentStep).show();
                $(".survey_overlay").fadeOut();
            } else {
                $("#step_"+currentStep).hide();
                $("#register_end").show();
                $("#g-recaptcha-wrapper").slideUp();
                $(".survey_footer").fadeOut();
            }
        }).fail(function () {
            $(".survey_overlay").fadeOut();
            $('#no_captcha').slideDown();
        });
    }

    function checkFirstStep() {
        if (0 == $("#name").val().length) {
            $("#name").addClass("invalid");
            $("#no_name").slideDown();
            return !1;
        }
        if (0 == $("#email").val().length) {
            $("#email").addClass("invalid");
            $("#no_email").slideDown();
            return !1;
        }

        var e = new RegExp(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.[A-Z0-9]{2,6}([-.][A-Z0-9]{2,6})*$/i);
        if( 0 == e.test($("#email").val()) ) {
            $("#email").addClass("invalid");
            $("#bad_email").slideDown();
            return !1;
        }
        else {
            $(".survey_overlay").css("display", "flex").hide().fadeIn();
            return register();
        }
    }

    function calcuateReward() {
        var e = 0;
        $("#q11, #q12, #q13, #q14, #q15, #q16, #q21, #q22, #q23, #q24, #q25, #q26, #q31, #q32, #q33, #q34, #q35, #q36").each(function () {
            if($(this).val() && 0 != $(this).val().length)
                e++;
        });

        e = Math.floor(e / 6);
        e *= 1e3;

        return e;
    }

})();